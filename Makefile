LATEXMK=latexmk -pdf -biber
.PHONY: mk
mk:
	pdflatex camlboot.tex
	biber camlboot
	pdflatex camlboot.tex
	pdflatex camlboot.tex

clean:
	latexmk -c
	rm -f *.{log,bbl,nav,rev,snm,vrb,vtc}

camlboot-T-diagrams.pdf: mk
	pdfjam -o camlboot-T-diagrams.pdf camlboot.pdf '18-19'

.PHONY: all clean
