# Note: ICFP'21 asks authors to suggest potential reviewers for their paper.

In the ICFP'21 Program Committee: Ben Greenman, Norman Ramsey
  (in general, people with language-implementation experience) 

Outside: see below.

A natural pool of reviewers would be people familiar with the OCaml
implementation. Unfortunately, we are in conflict with many people in
this group. We would suggest the following potential reviewers:

- Leo White <leo@lpw25.net> or Stephen Dolan <stedolan@stedolan.net>
- Alain Frisch <alain.frisch@lexifi.com> or Nicolás Ojeda Bär <nicolas.ojeda.bar@lexifi.com>

Another natural pool would be the people that participate to the
Bootstrappable project. Unfortunately, we are also of the impression
that many of those would could review the work are in conflict,
being close collaborators on the Guix project. (Those are informal
rather than institutional conflicts.)

Finally, within the ICFP community, we think this paper could be of
interest to people involved in language implementations. For example:

- Racket: Matthew Flatt, or other heavy contributors to the Racket
  implementation: Robby Findler, Eli Barzilay, Sam Tobin-Hochstadt,
  Jay McCarthy, Vincent St-Amour, Ryan Culpepper, Asumu Takikawa, etc.

- GHC: Richard Eisenberg, or other GHC contributors in
  more-or-less-academia (Simon or Simon, Ben Gamari, Joachim Breitner,
  Matthew Pickering, etc.).

- Scala (Nicolas Stucki, Martin Odersky), F# (Don Syme), Hack
  (Andrew Kennedy), Idris (Edwin Brady), etc.
