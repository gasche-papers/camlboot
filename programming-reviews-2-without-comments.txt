
Review #7A
===========================================================================

Paper summary
-------------
Debootstrapping the OCaml compiler, version 4.07.

Comments for author
-------------------
Your paper is a straightforward description of the project you undertook. Most of the parts of your previous submission that I found troublesome appear to have been revised or removed. Although I believe you have described thoroughly your treatment of the bytecode interpreters, in the end I felt like you had given it less attention than the compiler. Perhaps this is because the discussion is interspersed with the compiler treatment description. This is not a fatal flaw in the paper, but it does hinder readers from getting all the lessons when they want to apply your approach to a programming system (as opposed to programming language, which is your focus).

There are some trivial things I would suggest but not require:

* check your use of en-dashes to indicate numeric range; there might be only one instance.
* check the places where you have hyphenated an adverb/(adjective or noun) pair - you can find them by searching for “ly-”.


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


Review #7B
===========================================================================

Paper summary
-------------
I was Reviewer B in the previous round, and I am still happy with the paper overall. There are some typos that need fixing however, which I list below.

Comments for author
-------------------
(p means pages, S means section, P means paragraph, L means line, and -x means counting backwards from the last one)

#### Change ‘bootstrap’ to ‘bootstrap binary’

The cover letter says that ‘we stopped using "bootstrap" as a name, and use "bootstrap binary" instead’, but I still spot some occurrences of bootstrap as a noun.  (It seems most of them occur before the definition of ‘bootstrap binary’ in S1.2.1 — is it actually deliberate?)

* Abstract, Context, L2, ‘bootstrap’
* Abstract, Context, L6–7, ‘those bootstraps’
* Abstract, Inquiry, L2–3, ‘the bootstrap’
* Abstract, Grounding, L4, ‘existing bootstrap’
* p2, P2, L3, ‘a bootstrap’
* p2, P2, L-2, ‘this bootstrap’
* p2, P3, L-1, ‘later bootstraps’
* p4, L3, ‘a bootstrap’

#### Remove hyphens between adverbs and adjectives

* p2, P2, L4, ‘slightly-older’
* p7, L5, ‘clearly-written’
* p11, L-1, ‘natively-compiled’
* p14, S4, L2, ‘reasonably-small’
* p17, P3, L2, ‘mutually-recursive’
* p18, P3, L3, ‘precisely-defined’
* p21, L4, ‘naively-compiled’

#### References

* Capitalise proper nouns: “guix”, “rust”, “c++” (the double plus doesn’t look right btw — compare with the occurrences of “C++” in the main text), “Gnu guile”, “haskell”, “jdk”, “java”
* Include DOIs or URLs as text, not just hyperlinks
* Inconsistent formatting of conference names: full name (“6 th European Lisp Symposium”; and there’s an extra space in “6 th”), only acronym (“OOPSLA”), full name and acronym plus year (“ACSAC’05”; and the page numbers “13–pp” should be corrected)
* “ACM Turing award lectures” -> ACM Turing Award Lectures

#### Other

* Abstract, Approach, L2, ‘The new implementation needs not..., it suffices that...’: comma splice
* Abstract, Importance, L3, ‘programming-language research community’ -> programming language research community
* p2, P2, L-1, ‘the the designer’s’
* p2, P3, L-3: delete the comma (between ‘malicious logic...’ and ‘that’)
* p3, Footnote 2, ‘artefacts’ -> artifacts
* p5, L6, ‘naïve’ -> naive (as elsewhere in the paper)
* p5, S1.5, P2, L2, ‘native implementation’: too much space between the two words
* p12, P-2, ‘One notable limitation was..., the implementation would...’: comma splice
* p15, L3, ‘programming-language semantics’ -> programming language semantics
* p15, P-3, L-3, ‘intrisic’ -> intrinsic
* p15, P-2, L2–3, ‘We catch this expression’ -> We catch this exception
* p15, L-1, ‘parsetree’ -> parse tree
* p16, L3, ‘parsetrees’ -> parse trees
* p17, P2, L-5, ‘in presence of’ -> in the presence of
* p17, paragraph under Lesson 1, L3, ‘semantics difficulties’ -> semantic difficulties
* p22, S6.1, third bullet, ‘2h30’ -> 2h30m
* p22, S6.1, L-2: delete ‘what are’
* p23, S7, L1, ‘that’ -> which


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


Review #7C
===========================================================================

Paper summary
-------------
This paper describes the effort to Debootstrap the OCaml language: provide an implementation that can be compiled only from sources, without using any opaque, and therefore potentially risky, binaries. The project took a novel approach: instead of an "archaeological" approach taken in previous work, where the bootstrapping procedure is essentially duplicated by starting with an early compiler and using it to successively build later versions, a "tailored" approach builds an implementation from scratch. The strategy used included a compiler for a simple subset of the language and a reference interpreter for the (nearly) full language written in that subset. A number of insights are given about how to do "tailored" debootstrapping, and about some unanticipated benefits from doing so.

Comments for author
-------------------
This is a well-written Art paper. It does have substantial technical content, describing the designs of various artifacts with interesting details; this contribution could be compared to a conventional "systems" paper. But the validation is not the kind of empirical evaluation you might find in that kind of paper, aside from a few numbers showing build times; rather, it's more qualitative, describing the approach taken and its benefits in practice. This is done well; in my view, the reader who is potentially interested in debootstrapping another language would find both some motivations for doing so and helpful guidance about how to accomplish it in a way that is efficient in terms of human time. I therefore recommend the paper for publication in the journal.

Section 1 is fairly long and serves as an overview of the entire approach, along with a history of comparable approaches.  The observation about type erasure making debootstrapping easier is a nice one!  I appreciated your explanation about my question regarding decreased assurance (and others) in your cover letter.

The diverse double-compilation strategy is another very interesting technical approach from this section. The points made about a clean reference interpreter also make a lot of sense and give the reader a takeaway. The history is interesting as well, providing contrasts to the approach taken later in the paper.

Interesting tradeoffs are discussed in the implementation, including the decision to implement functors so more of the standard library could be used (making the reference interpreter realistic). Another is the choice to extend the compiler vs. simplify the interpreter and the time savings that provided.  The comparison to reimplementing in Scheme was small but interesting, from performance, engineering, and communication standpoints.  Finally the discussion of improvements to the build plan, including the value of compiling the interpreter, was quite cool.


* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *


Review #7D
===========================================================================

Paper summary
-------------
This paper describes work to provide a build path for the OCaml 4.07 compiler, which is written in OCaml, that does not rely on a binary built by a previous version. Instead, the approach combines a full self-interpreter written in a mini-language with a compiler for the mini-language, written in Scheme. Various technical components of the approach taken are described, and some performance measurements are presented.

Comments for author
-------------------
I was one of the reviewers in the previous round, so will focus on what has changed.

Overall, the authors have made a serious effort to accommodate the review comments, and the paper is much improved. I see no obstacles to accepting it.

The following are a few minor comments, mostly responding to points in the cover letter.

Despite some misunderstanding about what I was recommending terminology-wise, I am happy with the end result, using "bootstrap seeds" and "bootstrap binary" to good effect.

There was an open issue of how to describe the exercise of bootstrapping and its goals. In the cover letter, the authors write: '"minimising the trusted base" is also not clear: in what sense is a source-based build bath more "minimal" than one using a bootstrap binary? (Probably not in number of bytes on disk, for example.)' The short answer is that it's more minimal in the number of applications of any unauditable binary tool (in this case, the trusted bootstrap binary). Counting bytes on disk does not distinguish auditable from non-auditable bytes, which overlooks a key purpose of bootstrapping. I'm sure the authors know this, so there seems to be a misunderstanding. The text in the paper now appears clear enough about these issues.

T diagrams: yes, some diagram is good to have here and I can see the argument of prior reviewers. It would still be good to provide a reference to the notation, and/or perhaps to explain inline the distinct meaning of trapezium-headed versus rectangle-headed blocks, since that what what stuck out the most for me.

I like the new experiments with the Scheme interpreter... always interesting to find something different from expectations!

On p9 as seen in the LaTeX diff, "Java" should probably say "OpenJDK"

About the bibliography: capitalization is awry in several places and there is a stray space in '6 th'. For a couple of (non-software) entries, the publisher name is missing; this is an important field. Other issues: "pages 13-pp" looks wrong. For articles/artifacts referenced by URL, it is good to add a "retrieved on" date. I would recommend expanding URLs as text, rather than putting the link behind the title, although I'm not sure what the <Programming> house style has to say about that.
