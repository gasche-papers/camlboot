set +x
set -e

mkdir supplementary-material || { echo "error, you need to (rmdir supplementary-material)"; exit 1; }

cd supplementary-material

git clone git@github.com:Ekdohibs/camlboot.git
(cd camlboot; git submodule init; git submodule update)

# Note: <Programming> submissions are not anonymous, so we just include the git repository as-is.

# # file used to rewrite Copyright notices
# cat > to-censor.txt <<EOF
# literal:Nathanaël Courant==>anonymous
# EOF
# cd camlboot
# # anonymize the main repository
# git filter-repo \
#    --replace-text ../to-censor.txt \
#    --name-callback 'return b"anonymous"' \
#    --email-callback 'return b""' \
#    --message-callback 'return re.sub(b"Merge pull request (#[0-9]+) from .*", b"Merge pull request \\1 from anonymous", message)' \
#    --message-callback 'return re.sub(b"gasche", b"anonymous", message)'

# # turn the submodule into an independent git repository; doing this nicely is a bit tricky!
# git submodule deinit ocaml-src
# rm -fR .git/modules .gitmodules
# git clone --branch 4.07 --single-branch --depth 100 https://github.com/ocaml/ocaml ocaml-src
# cd ocaml-src
# git checkout -b 4.07-camlboot
# git remote add camlboot https://github.com/Ekdohibs/ocaml
# git pull camlboot 4.07-camlboot
# git remote remove camlboot
# rm -fR .git/logs .git/FETCH_HEAD # erase all traces of the camlboot remote
# cd ..

# # anonymize our commits in the ocaml-src subrepository
# cd ocaml-src
#   # we have to --force because this is not a fresh clone anymore
# git filter-repo --force \
#    --name-callback 'return b"anonymous"' \
#    --email-callback 'return b""' \
#    --message-callback 'return re.sub(b"Merge pull request (#[0-9]+) from .*", b"Merge pull request \\1 from anonymous", message)' \
#    --partial --refs origin/4.07..HEAD
# cd ..

# cd ..
# rm to-censor.txt

cat<<EOF > README.md
This archive contains artifacts for the submission

"Debootstrapping without archeology: Stacked implementations in Camlboot"

The following content is included:

- the git sources of camlboot
  also publicly available  at
    https://github.com/Ekdohibs/camlboot

- the Guix recipe to build a debootstrapped OCaml 4.07 using camlboot
  also publicly available at
    https://issues.guix.gnu.org/46806
EOF

mkdir guix-package
cp ../camlboot.scm guix-package
cd ..

rm -f supplementary-material.zip
zip -r supplementary-material supplementary-material
