We wish to thank the reviewers for their various feedback on our work;
we hope to use it to improve our presentation significantly.

We would summarize the salient points of the reviews as follows:

- Reviewer A believes that this article is not on-topic for ICFP; this
  seems to be the main ground for the recommendation to reject it.

- Reviewer B remarked our lack of insistence on the importance of
  debootstrapping as a problem. We realize that we over-reacted
  (out of cynicism or self-deprecation) in describing its interest,
  will answer to this specific point in our review and amend the paper
  accordingly.

- Reviewers A and C were dissatisfied with some of our presentation
  choices; their precise feedback helps us tell what aspects of our
  work they would have wanted to hear more about (or explained better;
  the point on the fact that our "lessons" were blandly written is
  well-taken), and we will improve our paper accordingly.

Our strategy in this response is to convince reviewer A to not
"strongly reject" the paper anymore, but instead "strongly support"
its *conditional* acceptance. We are convinced that with strong
shepherding (in the spirit of the two-step publication process of
PACMPL as a journal) and dedicated full-time effort on our part, we
can realistically achieve a version of the paper that will make the
reviewers satisfied and proud. For this the page limit imposing
half-size to Experience Reports (compared to normal papers) is
a strength: it's much easier to iterate on a smaller paper, and more
realistic to expect notable changes during the second phase.

Our review starts by discussing the first two points mentioned
above. Then we include detailed answers to the specific comments and
questions of our reviewers; this is well beyond the recommend word
limit, but we believe it is also respectful of our reviewers to
consider their points seriously.


## This paper is a good fit as an Experience Report for ICFP

Reviewer A writes:

> Some people, of whom I am one, will be keenly interested in the work
> described here.  But it's not an ICFP Experience Report.  I won't
> repeat the entire song and dance from the call for papers, but an
> Experience Report is meant to contribute to "a body of published,
> refereed, citable evidence that functional programming really works –
> or [that describes] what obstacles prevent it from working."
> I don't see that here.

In our experience, experience reports presented at ICFP have
a sensibly broader range than what is described in the call for
paper. (This portion of the Call for Paper is identical to the one
from ICFP 2015 http://icfpconference.org/icfp2015/cfp.html ; we
suspect that such text is generally copied verbatim without reflecting
changes in our scientific community.)

We looked at the Experience Reports accepted in the last five-or-so
years, and while some papers on teaching were clearly in the described
scope, many papers were not focused on the (somewhat quaint) goal of
convincing someone's manager to adopt functional programming. In
particular, one of our favorite experience reports from the last few
years is [Rebuilding Racket on Chez Scheme
(Experience Report)](https://dl.acm.org/doi/10.1145/3341642), in 2019,
which is much closer to the themes of our paper. Abstract below:

> We rebuilt Racket on Chez Scheme, and it works well&mdash;as long as
> we're allowed a few patches to Chez Scheme. DrRacket runs, the
> Racket distribution can build itself, and nearly all of the core
> Racket test suite passes. Maintainability and performance of the
> resulting implementation are good, although some work remains to
> improve end-to-end performance. The least predictable part of our
> effort was how big the differences between Racket and Chez Scheme
> would turn out to be and how we would manage those differences. We
> expect Racket on Chez Scheme to become the main Racket
> implementation, and we encourage other language implementers to
> consider Chez Scheme as a target virtual machine.

If a paper on the SqueakVM implementation (recommended as related work
by reviewer A) was a good fit for OOPSLA, why wouldn't a paper on
OCaml debootstrapping be appropriate for ICFP?

Note in particular that:

1. Our work is concerned with the problem of implementing functional
   languages "efficiently" (for an unusual notion of efficiency),

2. It emphasizes the importance of language design (of the language
   implemented, but also of the intermediate language we introduced on
   the way), in particular the type-erasure property,

3. We needed creative combinations of (basic) implementation
   techniques routinely covered at ICFP,

4. We wrote a hybrid system in two different functional programming
   languages (MiniML and Guile), intimately linked to the
   implementation of OCaml, one of the successful functional
   programming languages,

5. Simplicity and clarity were core guiding principles in our work,

6. Our contribution initiates an interaction with a project
   (bootstrappable builds) gathering a different, non-academic
   community of free software enthusiasts.

Finally: we believe that it would also be natural to ask the Program
Chair for guidance on this specific question.


## On the non-academic interest in debootstrapping

Reviewer B comments:

> - By the paper's own admission, the rationale to avoid bootstrapping
> is mostly of academic interest.

We may have undersold the importance of debootstrapping in our
writing. (To us, the fact that a large group of independent volunteers
is willing to work on this topic suggests that there is something to
it beyond academic interest -- in fact the vast majority of this work
has happened outside academia.) We will rephrase the paper.

Debootstrapped builds are important in practice for reasons similar to
reproducible builds. If a provider of binary software packages is not
trusted, or potentially the target of an attack, the public should be
able to rebuild the software themselves, and check that the result is
identical. However, even when the software is "reproducible" in this
sense, this is achieved in most cases by building using an identical
build environment provided by the untrusted provider. If the
environment itself contains a version of the software you are trying
to rebuild, how can you trust your own results? What if the build
environment is the one that's corrupted? You would reproducibly get
the same compromised result.

A debootstrapped build environment for the software provides clear
provenance tracking that, even though it cannot mitigate an attack,
provides a clear path to understanding where the attack came from, and
what was the impact. Reproducible builds have been on the radar for
about a decade in the free software community, debootstrapping is
a more recent concern. Although this kind of concern is not yet very
well-known, some important projects such as Bitcoin Core adopt
a debootstrapped (what they call "bootstrappable") toolchain to provide
more guarantees and transparency on the binary they provide.

For an article on bitcoin-core specifically, see:
https://bitcoinmagazine.com/technical/guix-makes-bitcoin-core-development-trustless


## Detailed responses to reviewer comments and questions

### Review A

> Some people, of whom I am one, will be keenly interested in the work
> described here.  But it's not an ICFP Experience Report.  I won't
> repeat the entire song and dance from the call for papers, but an
> Experience Report is meant to contribute to "a body of published,
> refereed, citable evidence that functional programming really works –
> or [that describes] what obstacles prevent it from working."
> I don't see that here.

(We addressed this point in the main section.)

> I want to know more about MiniML and about the choices and
> tradeoffs in its design.  Once we know the language design, writing an
> interpreter in MiniML and writing a compiler for MiniML are not so
> hard.  The language design is the crucial part.

We tried to describe the design principle for MiniML in Section 5.1,
"Defining the scope of MiniML". It is evident from the feedback here
that this section needs to be rewritten to be clearer. Let us expand
here:

- MiniML started with what many people would consider a canonical "toy
  ML": first-class functions, mutual recursion, variant/sum types and
  records. One notable limitation was on pattern-matching on
  variants/sums, the implementation would restrict to shallow patterns
  (no nesting of sum constructors) to avoid implementing
  pattern-matching compilation, a bulky feature.

  (In fact there was a more restricted version of MiniML in our first
   experiment, meant to be compiled to C instead of the OCaml
   bytecode, that would not support closures.)

- Then we added features gradually to cover what had been used in the
  interpreter (implemented in parallel; having MiniML be a subset of
  OCaml means we can always use the OCaml compiler itself). We
  iteratively reviewed the language features we used outside that
  subset, and for each decided whether to add them to MiniML or to
  remove them from the interpreter codebase.

  Note: in some cases features were motivated not directly by the
  interpreter codebase, but by their usage in the OCaml standard
  library for modules we wanted to use in the interpreter. In
  particular, we implemented functors to be able to use the Set, Map
  modules from the OCaml standard library.

We could fill this section with a more precise specification
(although it is not set in stone, our MiniML in two years may have
more features), or at least a list of salient features. Many features
are essentially syntactic sugar (for example the ability in the `fun
p -> ...` form to abstract over arbitrary patterns, rather than
just variables) or rely on the underlying OCaml runtime.

Moving from shallow patterns to full pattern-matching compilation was
probably the most time-consuming and invasive addition; it required
redesigning the Minicomp compiler from one-pass (from AST to bytecode
directly!) to a two-pass compiler, with patterns matching compiled
into lower-level control-flow constructs. This took about a full week
of work (using a not-too-naive compilation scheme), compared to
a couple hours to implement full pattern-matching (naively) in the
interpreter.

> The language design relates to a really interesting problem: "how do
> you design and implement a language which can be easily implemented
> over a small trusted computing base, and yet one in which it is
> pleasant to write a compiler."

If you ask us: you want recursive functions, algebraic datatypes and
pattern-matching, and static typing. There seems to be an element of
cultural preference here, and we suspect that different communities
will come up with a different answer to these questions (the SqueakVM
authors certainly did). So we would guess that our approach of carving
out a subset of the language we are already experts in is more likely
to be reused, possibly reinvented, than our specific language design
choices.

> A related question might be how to extend Scheme so the
> implementation of the MiniML compiler is not a continual source of
> frustration.

We have good things to say about Scheme in general and Guile in
particular, they are certainly nice languages. (The usability of Guile
is good, we found nice libraries for our needs, in particular the lalr
macro is very pleasant to use.) Our main source of frustration was the
absence of static typing. Writing a compiler involves a lot of choices
of data representation that create coupling between different parts of
the program (typically: the generator of some part of the IR, the
consumer of the same part of the IR); it happened time and time again
that we would change one of these data representations, and have the
code break in various places with errors that were always obvious
(and fairly immediate thanks to test coverage) but also always more
painful to understand and act upon that a proper typing error.

Typed Racket would definitely have been an improvement for this
(but there are less Racket users interested in debootstrapping than
Guile users, so it sounds like a more risky choice). A systematic use
of contracts would also have helped, and maybe something to consider
in the medium term for our project. We hope that eventually a Typed
Guile will exist.

>   - Likewise, your method has so much in common with the implementation of
>     the Squeak virtual machine that it would be a pity not to compare
>     them.  (Dan Ingalls, Ted Kaehler, John Maloney, Scott Wallace, and
>     Alan Kay. 1997. Back to the future: the story of Squeak, a practical
>     Smalltalk written in itself. In Proceedings of the 12th ACM SIGPLAN
>     conference on Object-oriented programming, systems, languages, and
>     applications (OOPSLA '97). Association for Computing Machinery, New
>     York, NY, USA, 318–326. DOI:https://doi.org/10.1145/263698.263754)

Thanks for the reference! We shall indeed include a discussion in our
Related Work section. 

There are similarities indeed, in particular the Squeak authors
emphasis of developing their system in Squeak/Smalltalk itself,
rather than a lower-level language, and their focus on getting a "good
enough" implementation in a "short enough" amount of time. However
there are also strong differences, discussed below.

The Squeak VM is implemented entirely in Smalltalk, with a small part
(the bytecode interpreter) implemented in a low-level subset that can
be translated to C. From the paper, it appears to be essentially
isomorphic to a subset of C, so a simple translation will do -- let us
call this fragment MiniSmalltalk for the purpose of the
discussion. From a distance this sounds similar to the situation with
our interpreter implemented in MiniML (our own language subset) that
is compiled to OCaml bytecode. But in fact the situation is very
different:

In the Squeak VM, the part that is written in this MiniSmalltalk
fragment is the *bytecode interpreter*, which is later translated to
C. This makes sense, a bytecode interpreter is low-level code that can
easily be written in a close-to-C fragment of your language, and it
brings strong performance improvements (compared to running their
bytecode interpreter in a Smalltalk implementation, possibly
a commercial one faster than Squeak at the time). But it does not
suffice to debootstrap the language: the compilation from Smalltalk to
its bytecode, which is a more subtle piece of software, remains
implemented in full Smalltalk, and requires a pre-existing VM image to
run. (There is a project to partially debootstrap Squeak:
https://github.com/yoshikiohshima/SqueakBootstrapper)

In contrast, in our work, the bytecode interpreter is the one from
OCaml, already implemented in C (so no need for
a translation/compilation step here), and the part written in MiniML
is a *full interpreter* for the OCaml language (the large fragment
used in the OCaml compiler codebase). Writing *this* piece of software
(for OCaml or Squeak) in a fragment isomorphic to C would be possible
but painful; this creates a strong motivation to make the
"translatable language" (MiniSmalltalk, our MiniML) more expressive,
more pleasant than C, by adding more features to it that make the
compilation (to C or to the OCaml bytecode) more difficult. If the
Squeak people wanted to go this route, they could gradually extend the
MiniSmalltalk fragment, turning the translation into C from a trivial
translation to an interesting compiler; then it would be much closer
to our work.

To summarize: in fact the SqueakVM implementation as described in this
excellent article is closer to the situation of the current OCaml
implementation: the "interesting" parts of the implementation are
written in the full language, with one specific part
(the bytecode interpreter) being written in a less-expressive,
low-level language (C, for OCaml) or fragment (MiniSmalltalk,
for Squeak). This does not suffice for debootstrapping, unless you are
willing to rewrite much larger portions of your codebase in a very low
level style (also a possible choice, as used by Racket for
a long while).

> At the bottom of page 3, you say that 74 hours of tedious work to
> replace the bootstraps is not practical.  Isn't two person-months more
> than that?  If less tedious?  Doesn't 74 hours look pretty good?  But
> wait!  Isn't 10 minutes unduly optimistic?  What claim are you trying
> to substantiate here?  If 74 hours is the optimistic number, what
> should it be compared with?

Yes, 74 hours is a very optimistic estimate, more like
a back-of-the-envelope calculation that we wanted to be sure not to
exaggerate. But even with this conservative number, we don't think
this approach is practical, even if it is faster on paper. You need an
expert in the OCaml compiler implementation that would be willing to
spend 74 hours of their life doing the most dreadfully tedious,
repetitive thing. It is much easier to find someone willing to spend
a couple weeks participating to a nice implementation of a simple
programming language, and it requires less expertise in the current
OCaml implementation. (It may be possible to convince an expert to do,
say, one tedious bootstrap a day, but then again you have to wait for
more than a year in total.) Finally, trying to make this process
reproducible by others (that is, not only redoing the bootstrap, but
storing intermediate creative source steps that turn "hard bootstraps"
into "easy bootstraps") would be even more work.

> In Section 1.3, how disappointed should we be that an entire
> implementation of Scheme has crept into your trusted computing base?
> Convince us that means progress.

Guile itself is compiled from C via a debootstrapped path, except for
the macro expander. The macro expander is implemented with macros, so
the sources contain the expanded version (generated guile code) to be
able to compile Guile. This means "progress": our binary surface
reduces from the bytecode version of a full OCaml compiler to
relatively readable guile code for macro-expansion. Guile maintainers
are aware of this remaining bootstrapping issue. After we submitted
our article, they have been working with Mes developers who managed to
bootstrap their own macro expander.

At a more meta level: the Guile+Guix people are practically the only
community currently interested in debootstrapping. We believe that if
one relatively-high-level-language manages a complete debootstrap in
the next few years, it will be these people -- and then we would
automagically benefit from this result. This is a social argument,
rather than a scientific one; but then, Scheme also seems to be a very
sensible (scientific) choice to use as a compromise between
debootstrappable minimalism and expressiveness to pleasantly implement
program-manipulating programs.

> At the end of section 1.3, please compare the 4 hours to build the
> compiler using your tool chain compared with the number of seconds or
> minutes need to build it using the binary bootstraps.

Indeed, we should add this for comparison. On one of our machines,
a parallel build of the OCaml distribution takes 1m42s, for 6m17s of
total user time. (This includes the time of building the native
compiler, which is not required to reproduce the bootstrap executable;
building just the core bytecode tools takes 0m36s, for 2m49s of total
user time).

> I'm really interested in what has to change in your interpreter and
> compiler to go from OCaml 4.07 to OCaml 4.12.

We have not yet completed the work of moving from 4.07 to 4.12, but here
is what we understand so far:

- The compiler codebase and standard library evolved from 4.07 to
  4.12, which requires matching changes to the part of our interpreter
  that interfaces with the OCaml runtime (by re-exporting runtime
  intrisics used in the compiler implementation itself; typically the
  handling of exception backtraces has changed from 4.07 to 4.12,
  requiring routine changes to our code that we have
  already performed).

- Given that our interpreter itself uses parts of the OCaml standard
  library, changes to the OCaml standard library may require extending
  MiniML -- or working around a module that has become too
  demanding. We haven't found an instance of this so far in our
  migration.

- The change of parser generator, from C-implemented ocamlyacc to
  OCaml-implemented Menhir, will have a large impact on our build plan
  and build scripts (which are a bit gnarly and consume a fair amount
  of our time). We have already checked that OCaml 4.07 builds recent
  versions of Menhir just fine, so we can generate the OCaml 4.12
  parser from our debootstrapped 4.07.

  Ideally we would like to maintain a debootstrapping suite that is as
  nice, lightweight and maintainable as reasonable, and avoid dealing
  with legacy software -- as OCaml 4.07 will eventually be
  regarded. So we see building Menhir with 4.07 as a temporary
  solution only, for example to re-prove the absence of trusting trust
  attack.

  To get rid of 4.07 as a legacy intermediate step, we plan to
  interpret the Menhir sources with our interpreter (which might
  require small extensions). Note: a parser generator needs first to
  parse the (.mly) grammar file; Menhir itself avoids needing
  a bootstrap by keeping its own .mly grammar compatible with
  ocamlyacc. So our better build plan would be:
  - build ocamlyacc from C
  - use it to compile Menhir's grammar
  - interpret the Menhir sources (including its generated grammar) to
    compile the OCaml parser to OCaml code
  - continue as currently (interpret the OCaml sources, including the
    generated parser code)

> I'd love to know why you included functors in MiniML.  Are they that
> useful for writing `interp`?

Functors are important because the implementation of generic sets and
maps from the OCaml standard library uses them. We want to be able to
reuse the OCaml standard library in our interpreter code as much as
reasonably possible. An alternative would be to implement our own
specialized sets/maps datastructures to not use functors (and in fact
we did this in an intermediate version of our code). But while this
makes debootstrapping slightly easier, it increases the maintenance
burden on the project (we duplicate code from stdlib), and it makes it
"weird" as an OCaml project. We care very much about the fact that our
interpreter can be taken as-is as a "good reference interpreter for
OCaml", easy to maintain and usable by other people not interested in
bootstrapping . Including weird downscaled copies of standard library
modules would weaken that goal.


### Review B

> - The actual lessons to be learned from this exercise were fairly
> generic and not particularly actionable.

We apologize for the somewhat-bland writing of these "lessons", and
would be happy to improve them with some feedback or advice. This
being said, we suspect that many of the languages within the ICFP
community, even the somewhat-mainstream ones, do not currently follow
these general recommendations, and it may inspire some of their users
to change things in this direction.

(For us working on the OCaml compiler, we were fortunate to meet
Debian developers deeply involved in the reproducible-builds
initiative years ago, which let us understand their concerns and work
to improve our corner of the ecosystem.)

We further discuss this question of how to better express our lessons
in response to reviewer C comments on this topic, below.

> - By the paper's own admission, the rationale to avoid bootstrapping
> is mostly of academic interest.

(We addressed this point in the main section.)


### Review C

> In general, the paper tends to discuss _how the authors arrived at
> their final product over time_ instead of describing the product and
> sharing the conclusions.

Our writing may give the wrong impression, but the paper describes the
final state of our debootstrapping toolchain, not the several
different iterations we went through to reach it. (This is explicitly
pointed out in the last sentence of the "Key metric" paragraph of the
introduction". An earlier design that compiled to C instead of the
OCaml bytecode is briefly mentioned as we thought it could be of
interest to know that compiling to a pre-existing bytecode works
better in practice than compiling to C. Our time estimates are only
for the final design, not the total amount of work. Our idea is to
propose a clear path to people interested in debootstrappingg
a different language, to benefit from our final design rather than our
failed attempts.

> To its credit, the paper does give two lessons in section 4. But
> lesson 1 is not supported with any details in the paper itself,

We give a detailed discussion of one specific design issue in Appendix
B.2. Experience report have half the page-size limit of normal papers,
so we had to make some difficult choices.

> and lesson 2 is one that every standards committee knows about.

Is this really the case, and is it relevant for functional languages?

- Among the programming languages in active development within the
  ICFP community (which are also important for everyone else), Scheme
  and SML are the only ones with a design-by-committee process
  (Haskell 98 also was this way, but people use GHC
  Haskell nowadays). Arguably one could add Javascript to the list,
  and WebAssembly. Still, the Scala, Haskell, F#, Coq, Agda, Idris
  people may be interested in some comments that would appear obvious
  to committees?

- We are less familiar with actively-used languages designed by
  committee, but is there really a wide-spread understanding of the
  format of *build artifacts* produced by language implementations? In
  the C world, the historic separation between compilers and linkers
  may have helped nudge compiler authors toward portably-specified
  artifact formats (the trend towards link-time-optimization probably
  reduces this, but then it's an entirely optional feature of
  the language), but we haven't previously heard of discussions of
  this topic. Would the reviewer have more precise pointers to
  documents discussing this point?

> There have got to be more insights from the adventure that would be useful to
> language designers.

For us, two insights that we found interesting (feedback welcome) were the following:

- Keeping to a type-erasure semantics really pays off for this. We
  would probably not have attempted this project if it had meant
  reimplementing a type-checker for OCaml.

- (this is rephrasing of Lesson 1, hopefully less trite?) It is
  surprisingly easy, when evolving a language from the point of view
  of compiler-based implementations, to design, discuss, evaluate and
  integrate features described in terms of their
  compilation/elaboration semantics, without realizing that their
  direct operational semantics (as a rewrite relation, in
  a reference interpreter) is subtle and may be delicate. We have seen
  this happen with OCaml, but we suspect that other languages,
  including designed-by-committees language, may have been guilty of
  this for some features.

  Note: In the case of module aliases that we discuss in details, this
  happens for a feature at the boundary between the language
  constructs proper and the surrounding tooling
  (dependency management, linking semantics, etc.); we suspect that
  issues may lie in this grey area.


> Additionally, I would like to see:
>
> + a list of specific features that posed challenges and their implications for
>   other functional languages;
> + general lessons for debootstrappers (in addition to lessons for language
>   designers / implementors); and
> + concrete evidence for the many vague claims in the paper (see Other Comments
>   below for some).

We warmly thank the reviewer for the precision of their comments,
which will be invaluable in improving the presentation. We tried to
answer the Other Comments points in detail, to demonstrate that our
claims could be made less vague and more convincing with relatively
little effort (in scope for a minor revision).

> It would also be good to list the missing features in `interp`, so that
> researchers who might want to build on your interpreter know its limitations.

We implemented OCaml features "on demand" by iteratively trying to run
the compiler sources, which would fail on unsupported features. If we
had been able to run the bytecode compiler, we would probably have
left objects (used only in the native-compiler backend) unsupported,
as interpreting them was a fair amount of work

Looking for calls to the `unsupported` function in interpreter/eval.ml
(we gave an anonymized source repository with our submission, there is
a non-anonymous version at
https://github.com/Ekdohibs/camlboot/blob/f3949c/interpreter/eval.ml ),
we see the following main missing features:

- lazy patterns ((lazy p) will force its scrutinee and match its value with (p))
- a couple minor pattern features (polymorphic variant type patterns #foo,
  local module open in patterns M.(p))
- direct object expressions (outside a class declaration)
- recursive modules

Most of these features are fairly minor and could be added in an hour
or so, we just didn't need them to interpret the compiler
codebase. Recursive modules may be more subtle, but we already handle
recursive dependencies between compilation units and recusive values,
so we don't expect any difficulties (so we would guess a few hours of
work, possibly a day if it proves more subtle than planned).

(Note: if that made any difference to the reviewers, we would be happy
to promise to implement those missing features during
a conditional-acceptance period.)


> #### Other Comments

> - Section 1.1 : In the end, I am not convinced that bootstrapping was
>   interesting to solve. It sounds like a lot of chores and frustrating
>   false starts.

We addressed the "pragmatic" interest in bootstrapping in the main
section above. We found it a very interesting problem to work
on. Besides, we are really interested in side-project usage of
a reference interpreter in OCaml; in particular, as mentioned in the
paper (Section 1.4), having a small and simple "reference"
implementation could be very useful for differential testing of
reference implementations. (For this reuse, by us and hopefully by
other people, to happen, it is important to keep the codebase as
simple and maintainnable as possible. This informed several of our
design and implementation decisions.)


> - Section 1.1 : Can you give any examples of bugs that survived
>   through bootstraps?

We heard through the grapevine (more precisely: from one of us
attending the reproducible-builds summit in 2018, were several
implementors were looking at debootstrapping) of an issue with
the handling of hexadecimal literals that survived through the
bootstrap of the Mes implementation of Scheme, and was found when
debootstrapping. It appeared as a failure to build GCC, after the
bug survived through compilation of Mes and several versions of
TinyCC.
Unfortunately, we have not been about to find a written account of
this bug.


> - Section 1.4 : What benefits of type erasure do you have in mind?

Type erasure lets you define an interpreter on the *untyped* abstract
syntax trees of your programs. Features that break type-erasures
(typically: Haskell type classes, Scala implicits, Rust traits, etc.)
have their runtime behavior depend on typing information, which in
general requires type-checking programs first and evaluating typed
derivation trees.

Type systems for languages within the ICFP community (languages that
forgo type erasures tend to have more complex type-systems
than average). Reimplementing the OCaml type system to a simpler
language fragment would have been a *massive* undertaking, at least
one order of magnitude more work than the total work spent on
camlboot.

An alternative might be to define an "explicit" version of the
surface syntax that can represent explicit resolution of
type-depending features -- explicitly elaborating type classes or
implicit arguments in the source -- which does satisfy type
erasure. Then convert the implementation of interest into this
explicit representation, and review it manually to correspond to
programmer intents, turning this version into a trusted "source". This
could be done as a one-time port, but maintaining this correspondence
as the reference implementation evolves sounds impractical.


> - Section 1.5 : A short related work would have been more useful
>   than the huge figure 1.

We hope to replace it with a smaller, more compact figure based on
T-diagrams, as per reviewer A's suggestion. This could let us
move the Related Work section from the appendix into the main article.


> - Section 3.1 : Why is it problematic that Menhir is written in OCaml,
>   especially since Menhir builds with 4.07?

Our interpreter starts by parsing the source code. As our interpreter
cannot depend on itself, things become delicate if the parser is
written in OCaml -- outside the MiniML fragment.

(For more details on how we plan to address this issue, see our reply
to review A above.)


> - Section 3.1 : Why would you rather update your tool for 4.12? What
>   benefits might the update offer?

The most common approach to debootstrapping tries to restore a chain
of "legacy" implementations going back to the pre-bootstrap era. This
creates delicate maintenance concern -- it is already a miracle to get
this old abandoned interprer project in C from the 90s working today,
this keeps getting harder to maintain as time passes.

We prefer the approach of building a new, clean
(if strategically naive) reimplementation for debootstrapping
purposes, to get a maintenable debootstraping system. The intention is
to maintain and evolve this codebase over time to stay up-to-date with
the reference bootstrapped implementation, and remain as clean and
maintenable as possible. (We believe this is especially beneficial as
the side-products of our debootstrapping work, in particular the
reference interpreter, may be used for unrelated interesting purposes if
they are actively maintained.)

We could freeze our project on OCaml 4.07, and build a stack of
bootstrap steps on top of it. This would be precisely the start of
a "legacy" bootstrap path that we want to avoid.


> - Section 4 says that `interp` supports "almost all" of OCaml and
>   section 5.1 says that your interpreter and compiler divide features
>   among themselves. These statements seem contradictory; what's in
>   each part?

`interp` does support "almost all" of OCaml (see our detailed response
above). The tension we mention in 5.1 exists between the feature set
supported by the compiler and the features *usable in the interpreter
implementation* (not the features supported by the interpreter).

For example, our first implementation of the interpreter (tested using
the full OCaml implementation) used OCaml polymorphic variants in
a couple places. We had to decide between adding them to our MiniML,
or *removing* them from the interpreter codebase. We made this kind of
choices by comparing the cost of adding each feature to the compiler,
and the maintenance cost of not-using the feature in the interpreter
codebase.


> - Lesson 1 : Wouldn't a language model give the same benefit?

We are not sure what you mean by "language model". If you mean
a small-step operational semantics (for example), then yes. But it is
socially/collectively difficult to maintain an up-to-date reference
semantics over time without making it executable (for testing, to have
something "break" in the real world when you forget to update it,
etc.), and an executable operational semantics is precisely
a reference interpreter.

> - Lesson 1 : Are all compiler features suitable for an interpreter? It
>   sounds like committing to a debootstrap forces one to address too
>   many details if the only goal is to understand the language.

Lesson 1 only claims that a reference interpreter is useful to reflect
on language design; it needs not be debootstrapped (it could be
implemented in the full language, etc.).


> - Section 5 : Why is the C code failure significant?

In our experience, people tend to assume that compiling to C will give
good results, in terms of getting a working prototype quickly *and* in
terms of performance. We therefore spend four lines to point out that
naively-generated (compiled) C code runs *slower* than
naively-generated (interpreted) OCaml bytecode. It could be
surprising to some people, therefore interesting.


> - Section 5 : What do we learn from the compiler code snippets? This
>   seems better in an appendix.

We claim throughout the paper that we strived to keep our approach
maintainable. (This is not just about the paper: maintainability of
the result informed several of our design and implementation
decisions.) The best approximation we found to assess this claim is to
show representative code snippets (for the compiler, here, and the
interpreter in appendix), to give readers an idea of the code style,
readability or complexity.

> - Section 7 : What rewards did you get from the debootstrapping?

We now know with strong confidence¹ that the OCaml implementation is
free from trusting-trust attack, which is a non-obvious result. To our
knowledge (as discussed in our Related Work appendix A), few of the
languages developed within the ICFP community have this guarantee.

¹: we say "strong confidence" because it is in theory possible that
a malicious snippet in the macro-expander of Guile would be designed
to affect the Guile implementation to introduce a malicious behavior
in our new OCaml interpreter to in turn reproduce (and thus
make undetectable) a trusting-trust attack within the OCaml
bytecode. We don't know how to achieve this as the reference
interpreter was written well after the Guile macro-expander, but this
might in theory be done with extremely advanced program
analysis. (Of course, a different attack through malicious source code
should also be considered.)

We also mentioned earlier that our approach to a "maintenable
debootstrapping codebase" is expected to yield other side-benefits for
the OCaml community. Maintaining this approach as the language and its
main implementation keep evolving, will force us and other OCaml
caretakers to take into account some of the lessons we learned --
possibly distilling some of the wisdom that would be obtained by
a multi-implementation language design committeee -- as well as
unlocking the reference interpreter as a byproduct.
