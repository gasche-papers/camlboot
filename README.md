A work-in-progress paper describing
[camlboot](https://github.com/Ekdohibs/camlboot), an approach to
[debootstrap](https://bootstrappable.org/) the OCaml compiler.
